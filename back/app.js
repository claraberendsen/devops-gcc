const Koa = require("koa");
const mount = require("koa-mount");
const auth = require("koa-basic-auth");
const services = require("./services");
const koaBody = require("koa-body");
const cors = require("koa2-cors");

const app = new Koa();

app.use(mount("/", services));

app.use(
  cors({
    credentials: true,
    allowMethods: ["GET", "POST", "DELETE", "PUT", "OPTIONS"],
    allowHeaders: ["Content-Type", "Authorization", "Accept"]
  })
);
app.use(
  koaBody({
    multipart: true
  })
);
module.exports = app;
