const Boom = require("boom");

const authorize = async ({ user, password }) => {
  if (!user) throw Boom.badRequest("El email es requerido");
  if (!password) throw Boom.badRequest("La contraseña es requerida");
  if (user !== "prueba@gcc.pol.una.py" || password !== "gcc123") {
    throw Boom.unauthorized("Contraseña o mail erroneos");
  } else {
    return true;
  }
};

module.exports = {
  authorize
};
